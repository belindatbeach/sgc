#General Introduction of Bitconnectcoin
Bitconnect Coin is a secure and energy efficient PoW/PoS coin. Bitconnect Coin uses a faster PoW distribution mechanism to distribute the initial coins, then after few days the coin is basically transferred to a pure PoS coin, where the generation of the coin is mainly through the PoS interests.

#Bitconnect Coin Specification

Maximum 28 million BitConnect coin will ever exist to the community.

sgccoin coin adopt a variable PoS rate that will be given periodically payout as per following list,
- 1st 6 months -> 60% (10% per month)
- 2nd 6 months -> 50% (8% per month)
- 3rd 6 months -> 40% (7% per month)
- 4th 6 months -> 30% (5% per month)
- 5th 6 months -> 20% (3% per month)
- 6th 6 months -> 10% (1.4 per month and ongoing.)

#How are they used?
The BitConnect coins are used by sgccoin community members for sgccoin services and to store and invest the wealth in a non-government controlled currency. The BitConnect coins will also be used  as payment system on number of BitConnect partner websites.

#How are they produced?
BitConnect coin uses a special algorithm called the POW/POS to secure the BitConnect Coin network. The moment you acquire BitConnect Coin it becomes an interest bearing asset with 120% return per year through PoS minting. All you have to do to earn with this method is to hold coins in your Bitconnect-QT wallet. In addition to PoS minting, BitConnect coin can be mined with CPU/GPU and does not need an ASIC miner like Bitcoin does.

#Download Wallet
<b>HOW TO SET UP SGCCOIN COIN WALLET ON WINDOWS OPERATING SYSTEM?</b>

A “BitConnect wallet” is basically the BitConnect Coin Account, which allows you to receive BitConnect Coin, store them, and then send them to others.

<b>Click here : http://www.sgccoincoin.co/wallet/sgccoin-wallet.zip</b>
- Unzip the wallets files.
- You will get sgccoin.exe file, Install the wallet software by double click on sgccoin.exe file.
- You can now send and receive BitConnect Coin directly from BitConnect Desktop Wallet and also use this wallet to stake BitConnect Coin.

<b> HOW TO SET UP SGCCOIN COIN WALLET ON LINUX OPERATING SYSTEM?</b> 

<b>Click here : http://www.sgccoincoin.co/wallet/sgccoin-linux-qt.zip</b>

- Open linux terminal and go to destination path of downloaded directory.
- Unzip the wallets files using command unzip wallet_file.zip -d destination_folder.
- execute the wallet file using command ./sgccoin-linux-qt.

<b>HOW TO SET UP SGCCOIN COIN WALLET ON MAC OS X OPERATING SYSTEM?</b>

<b>Click here : https://sgccoincoin.co/wallet/sgccoin-mac.zip?v=154545</b>

- Unzip the wallets files.
- You will get sgccoin-qt.dmg file, Install the wallet software by double click on sgccoin-qt.dmg file.
- You can now send and receive BitConnect Coin directly from BitConnect Desktop Wallet and also use this wallet to stake BitConnect   Coin.



